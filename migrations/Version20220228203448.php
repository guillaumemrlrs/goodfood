<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220228203448 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, ligne1 VARCHAR(100) NOT NULL, ligne2 VARCHAR(100) NOT NULL, ville VARCHAR(50) NOT NULL, codepostal VARCHAR(50) NOT NULL, INDEX IDX_C35F0816FB88E14F (utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cb (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT DEFAULT NULL, numero VARCHAR(20) NOT NULL, titulaire VARCHAR(50) NOT NULL, date_validite VARCHAR(10) NOT NULL, INDEX IDX_ACB52AEFFB88E14F (utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commande_client (id INT AUTO_INCREMENT NOT NULL, statut_commande_id INT DEFAULT NULL, adresse_id INT DEFAULT NULL, type_paiement_id INT DEFAULT NULL, datecreation DATETIME NOT NULL, INDEX IDX_C510FF80FB435DFD (statut_commande_id), INDEX IDX_C510FF804DE7DC5C (adresse_id), INDEX IDX_C510FF80615593E9 (type_paiement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commande_fournisseur (id INT AUTO_INCREMENT NOT NULL, restaurant_id INT DEFAULT NULL, fournisseur_id INT NOT NULL, date_creation DATETIME NOT NULL, INDEX IDX_7F6F4F53B1E7706E (restaurant_id), INDEX IDX_7F6F4F53670C757F (fournisseur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contenir_quantite (id INT AUTO_INCREMENT NOT NULL, commande_client_id INT DEFAULT NULL, menu_id INT DEFAULT NULL, plat_id INT DEFAULT NULL, panier_id INT DEFAULT NULL, quantite INT NOT NULL, INDEX IDX_4A1446B69E73363 (commande_client_id), INDEX IDX_4A1446B6CCD7E912 (menu_id), INDEX IDX_4A1446B6D73DB560 (plat_id), INDEX IDX_4A1446B6F77D927C (panier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fournisseur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(70) NOT NULL, tel VARCHAR(20) NOT NULL, email VARCHAR(70) NOT NULL, ligne1 VARCHAR(100) NOT NULL, ligne2 VARCHAR(100) NOT NULL, ville VARCHAR(70) NOT NULL, code_postal VARCHAR(15) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupement (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(70) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredient (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ligne_ingredient (id INT AUTO_INCREMENT NOT NULL, ingredient_id INT NOT NULL, commande_fournisseur_id INT NOT NULL, quantite INT NOT NULL, prix DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_8803966D933FE08C (ingredient_id), INDEX IDX_8803966DA2577AA5 (commande_fournisseur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, image LONGBLOB NOT NULL, disponible TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE panier (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT DEFAULT NULL, reduction_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_24CC0DF2FB88E14F (utilisateur_id), UNIQUE INDEX UNIQ_24CC0DF2C03CB092 (reduction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plat (id INT AUTO_INCREMENT NOT NULL, categorie_id INT NOT NULL, nom VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, image LONGBLOB NOT NULL, disponible TINYINT(1) NOT NULL, INDEX IDX_2038A207BCF5E72D (categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plat_compose_ingredient (id INT AUTO_INCREMENT NOT NULL, ingredient_id INT NOT NULL, plat_id INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, unite VARCHAR(25) DEFAULT NULL, INDEX IDX_335FC46C933FE08C (ingredient_id), INDEX IDX_335FC46CD73DB560 (plat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plat_dans_menu (id INT AUTO_INCREMENT NOT NULL, plat_id INT NOT NULL, menu_id INT NOT NULL, quantite INT NOT NULL, UNIQUE INDEX UNIQ_B77DF03BD73DB560 (plat_id), INDEX IDX_B77DF03BCCD7E912 (menu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reduction (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(30) NOT NULL, pourcentage INT NOT NULL, date_expiration DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL, groupement_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, tel VARCHAR(20) NOT NULL, email VARCHAR(50) NOT NULL, ligne1 VARCHAR(100) NOT NULL, ligne2 VARCHAR(100) NOT NULL, ville VARCHAR(70) NOT NULL, code_postal VARCHAR(20) NOT NULL, INDEX IDX_EB95123FE66695CE (groupement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant_plat (restaurant_id INT NOT NULL, plat_id INT NOT NULL, INDEX IDX_E22E3263B1E7706E (restaurant_id), INDEX IDX_E22E3263D73DB560 (plat_id), PRIMARY KEY(restaurant_id, plat_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant_menu (restaurant_id INT NOT NULL, menu_id INT NOT NULL, INDEX IDX_BF13AAF7B1E7706E (restaurant_id), INDEX IDX_BF13AAF7CCD7E912 (menu_id), PRIMARY KEY(restaurant_id, menu_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statut_commande (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_paiement (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, tel VARCHAR(20) NOT NULL, civilite VARCHAR(15) NOT NULL, UNIQUE INDEX UNIQ_1D1C63B3E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F0816FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE cb ADD CONSTRAINT FK_ACB52AEFFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE commande_client ADD CONSTRAINT FK_C510FF80FB435DFD FOREIGN KEY (statut_commande_id) REFERENCES statut_commande (id)');
        $this->addSql('ALTER TABLE commande_client ADD CONSTRAINT FK_C510FF804DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE commande_client ADD CONSTRAINT FK_C510FF80615593E9 FOREIGN KEY (type_paiement_id) REFERENCES type_paiement (id)');
        $this->addSql('ALTER TABLE commande_fournisseur ADD CONSTRAINT FK_7F6F4F53B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE commande_fournisseur ADD CONSTRAINT FK_7F6F4F53670C757F FOREIGN KEY (fournisseur_id) REFERENCES fournisseur (id)');
        $this->addSql('ALTER TABLE contenir_quantite ADD CONSTRAINT FK_4A1446B69E73363 FOREIGN KEY (commande_client_id) REFERENCES commande_client (id)');
        $this->addSql('ALTER TABLE contenir_quantite ADD CONSTRAINT FK_4A1446B6CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('ALTER TABLE contenir_quantite ADD CONSTRAINT FK_4A1446B6D73DB560 FOREIGN KEY (plat_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE contenir_quantite ADD CONSTRAINT FK_4A1446B6F77D927C FOREIGN KEY (panier_id) REFERENCES panier (id)');
        $this->addSql('ALTER TABLE ligne_ingredient ADD CONSTRAINT FK_8803966D933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
        $this->addSql('ALTER TABLE ligne_ingredient ADD CONSTRAINT FK_8803966DA2577AA5 FOREIGN KEY (commande_fournisseur_id) REFERENCES commande_fournisseur (id)');
        $this->addSql('ALTER TABLE panier ADD CONSTRAINT FK_24CC0DF2FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE panier ADD CONSTRAINT FK_24CC0DF2C03CB092 FOREIGN KEY (reduction_id) REFERENCES reduction (id)');
        $this->addSql('ALTER TABLE plat ADD CONSTRAINT FK_2038A207BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE plat_compose_ingredient ADD CONSTRAINT FK_335FC46C933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
        $this->addSql('ALTER TABLE plat_compose_ingredient ADD CONSTRAINT FK_335FC46CD73DB560 FOREIGN KEY (plat_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE plat_dans_menu ADD CONSTRAINT FK_B77DF03BD73DB560 FOREIGN KEY (plat_id) REFERENCES plat (id)');
        $this->addSql('ALTER TABLE plat_dans_menu ADD CONSTRAINT FK_B77DF03BCCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('ALTER TABLE restaurant ADD CONSTRAINT FK_EB95123FE66695CE FOREIGN KEY (groupement_id) REFERENCES groupement (id)');
        $this->addSql('ALTER TABLE restaurant_plat ADD CONSTRAINT FK_E22E3263B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_plat ADD CONSTRAINT FK_E22E3263D73DB560 FOREIGN KEY (plat_id) REFERENCES plat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_menu ADD CONSTRAINT FK_BF13AAF7B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE restaurant_menu ADD CONSTRAINT FK_BF13AAF7CCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commande_client DROP FOREIGN KEY FK_C510FF804DE7DC5C');
        $this->addSql('ALTER TABLE plat DROP FOREIGN KEY FK_2038A207BCF5E72D');
        $this->addSql('ALTER TABLE contenir_quantite DROP FOREIGN KEY FK_4A1446B69E73363');
        $this->addSql('ALTER TABLE ligne_ingredient DROP FOREIGN KEY FK_8803966DA2577AA5');
        $this->addSql('ALTER TABLE commande_fournisseur DROP FOREIGN KEY FK_7F6F4F53670C757F');
        $this->addSql('ALTER TABLE restaurant DROP FOREIGN KEY FK_EB95123FE66695CE');
        $this->addSql('ALTER TABLE ligne_ingredient DROP FOREIGN KEY FK_8803966D933FE08C');
        $this->addSql('ALTER TABLE plat_compose_ingredient DROP FOREIGN KEY FK_335FC46C933FE08C');
        $this->addSql('ALTER TABLE contenir_quantite DROP FOREIGN KEY FK_4A1446B6CCD7E912');
        $this->addSql('ALTER TABLE plat_dans_menu DROP FOREIGN KEY FK_B77DF03BCCD7E912');
        $this->addSql('ALTER TABLE restaurant_menu DROP FOREIGN KEY FK_BF13AAF7CCD7E912');
        $this->addSql('ALTER TABLE contenir_quantite DROP FOREIGN KEY FK_4A1446B6F77D927C');
        $this->addSql('ALTER TABLE contenir_quantite DROP FOREIGN KEY FK_4A1446B6D73DB560');
        $this->addSql('ALTER TABLE plat_compose_ingredient DROP FOREIGN KEY FK_335FC46CD73DB560');
        $this->addSql('ALTER TABLE plat_dans_menu DROP FOREIGN KEY FK_B77DF03BD73DB560');
        $this->addSql('ALTER TABLE restaurant_plat DROP FOREIGN KEY FK_E22E3263D73DB560');
        $this->addSql('ALTER TABLE panier DROP FOREIGN KEY FK_24CC0DF2C03CB092');
        $this->addSql('ALTER TABLE commande_fournisseur DROP FOREIGN KEY FK_7F6F4F53B1E7706E');
        $this->addSql('ALTER TABLE restaurant_plat DROP FOREIGN KEY FK_E22E3263B1E7706E');
        $this->addSql('ALTER TABLE restaurant_menu DROP FOREIGN KEY FK_BF13AAF7B1E7706E');
        $this->addSql('ALTER TABLE commande_client DROP FOREIGN KEY FK_C510FF80FB435DFD');
        $this->addSql('ALTER TABLE commande_client DROP FOREIGN KEY FK_C510FF80615593E9');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F0816FB88E14F');
        $this->addSql('ALTER TABLE cb DROP FOREIGN KEY FK_ACB52AEFFB88E14F');
        $this->addSql('ALTER TABLE panier DROP FOREIGN KEY FK_24CC0DF2FB88E14F');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE cb');
        $this->addSql('DROP TABLE commande_client');
        $this->addSql('DROP TABLE commande_fournisseur');
        $this->addSql('DROP TABLE contenir_quantite');
        $this->addSql('DROP TABLE fournisseur');
        $this->addSql('DROP TABLE groupement');
        $this->addSql('DROP TABLE ingredient');
        $this->addSql('DROP TABLE ligne_ingredient');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE panier');
        $this->addSql('DROP TABLE plat');
        $this->addSql('DROP TABLE plat_compose_ingredient');
        $this->addSql('DROP TABLE plat_dans_menu');
        $this->addSql('DROP TABLE reduction');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE restaurant_plat');
        $this->addSql('DROP TABLE restaurant_menu');
        $this->addSql('DROP TABLE statut_commande');
        $this->addSql('DROP TABLE type_paiement');
        $this->addSql('DROP TABLE utilisateur');
    }
}
