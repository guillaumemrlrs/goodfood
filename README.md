# GoodFood 2.0

GoodFood est un site de restauration en ligne développé avec le framework Symfony.

## Installation

Installer php 8.1.2 depuis ce lien :

https://windows.php.net/download/

Installer composer depuis ce lien :

https://getcomposer.org/download/

Attention à bien sélectionner le bon php.exe lors de l'installation de composer !

Se mettre dans le répertoire du projet :

```bash
cd workspace\”repertoireduprojet”
```

Pour cloner le projet :

```bash
git clone “url github du projet”
```

Pour installer composer dans le projet :

```bash
composer install
```

Pour installer Node.js :

https://nodejs.org/en/download/

Pour installer npm et yarn  : 

```bash
npm install
```

```bash
npm install --global yarn
```

```bash
yarn install
```

Pour bien afficher le CSS :

```bash
yarn encore dev
```

```bash
yarn encore dev –- watch
```

Pour lancer le serveur :

```bash
php -S 127.0.0.1:8000 -t public
```
